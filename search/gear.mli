open! Core

type t [@@deriving sexp_of]

type parent =
  | Root of Point.t
  | Same_axle of { parent : (t[@sexp.opaque]) }
  | Contact of
      { parent : (t[@sexp.opaque])
      ; angle : float
      }
[@@deriving sexp_of]

val create : name:string -> module_:float -> n:int -> parent:parent -> external_:bool -> t
val name : t -> string
val module_ : t -> float
val n : t -> int
val parent : t -> parent
val external_ : t -> bool

(** Radius of the pitch circle. *)
val pitch_radius : t -> float

(** Computed position of the gear. *)
val position : t -> Point.t

module Alternate_sexp : sig
  val sexp_of_t : t -> Sexp.t
end
