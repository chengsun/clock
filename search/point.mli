open! Core

type t =
  { x : float
  ; y : float
  }
[@@deriving sexp_of]

val origin : t
val ( + ) : t -> t -> t
val ( - ) : t -> t -> t
val ( * ) : t -> float -> t
val ( / ) : t -> float -> t
val of_angle : float -> t
val of_polar : angle:float -> radius:float -> t
val polar_radius : t -> float
