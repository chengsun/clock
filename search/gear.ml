open! Core

type t =
  { name : string
  ; module_ : float
  ; n : int
  ; parent : parent
  ; external_ : bool
  ; position : Point.t Lazy.t
  }

and parent =
  | Root of Point.t
  | Same_axle of { parent : (t[@sexp.opaque]) }
  | Contact of
      { parent : (t[@sexp.opaque])
      ; angle : float
      }
[@@deriving fields, sexp_of]

let pitch_diameter t = t.module_ *. float t.n
let pitch_radius t = pitch_diameter t /. 2.
let position t = Lazy.force t.position

let resolve_position t =
  match t.parent with
  | Root position -> position
  | Same_axle { parent } -> position parent
  | Contact { parent; angle } ->
    assert (Float.equal parent.module_ t.module_);
    let parent_position = position parent in
    let delta =
      Point.of_polar
        ~angle
        ~radius:
          (((if Bool.equal parent.external_ t.external_ then 1. else -1.)
           *. pitch_radius parent)
          +. pitch_radius t)
    in
    Point.(parent_position + delta)
;;

let create ~name ~module_ ~n ~parent ~external_ =
  let rec t = { name; module_; n; parent; external_; position }
  and position = lazy (resolve_position t) in
  t
;;

module Alternate_sexp = struct
  let sexp_of_parent parent =
    match parent with
    | Root point -> [%sexp "Root", (point : Point.t)]
    | Same_axle { parent = _ } -> [%sexp "Same_axle"]
    | Contact { parent = _; angle } ->
      [%sexp
        "Contact"
        , { angle = (Float.mod_float ((angle *. 180. /. Float.pi) +. 360.) 360. : float) }]
  ;;

  let sexp_of_t t =
    let p = Point.(resolve_position t / 10.) in
    [%sexp
      { name = (t.name : string)
      ; internal = (not t.external_ : bool)
      ; number_of_teeth = (t.n : int)
      ; pitch_diameter = (pitch_diameter t /. 10. : float)
      ; parent = (t.parent : parent)
      ; position = ({ Point.x = 8. +. p.x; y = 8. -. p.y } : Point.t)
      }]
  ;;
end
