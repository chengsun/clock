open! Core

type 'a t =
  | Const : 'a -> 'a t
  | Range_int : int * int * int -> int t
  | Both : 'a t * int * 'b t * int -> ('a * 'b) t
  | Map : 'a t * ('a -> 'b) -> 'b t

let return x = Const x

let rec weight : type a. a t -> int =
 fun t ->
  match t with
  | Const _ -> 0
  | Range_int _ -> 1
  | Both (_, w1, _, w2) -> w1 + w2
  | Map (t, _) -> weight t
;;

module Let_syntax = struct
  module Let_syntax = struct
    type nonrec 'a t = 'a t

    let return = return
    let both t1 t2 = Both (t1, weight t1, t2, weight t2)
    let map t ~f = Map (t, f)

    module Open_on_rhs = struct
      let return = return
      let range ~lo ~hi = Range_int (lo, hi, lo + ((hi - lo) / 2))

      let random_angle =
        map (range ~lo:(-36000) ~hi:36000) ~f:(fun deg -> float deg *. Float.pi /. 1800.)
      ;;
    end
  end
end

let rec perturb : type a. random_state:Random.State.t -> a t -> a t =
 fun ~random_state t ->
  match t with
  | Const x -> Const x
  | Range_int (lo, hi, cur) ->
    let cur' = min (max (cur + Random.State.int_incl random_state (-4) 4) lo) hi in
    Range_int (lo, hi, cur')
  | Both (t1, w1, t2, w2) ->
    (match Random.State.int_incl random_state 1 (w1 + w2) <= w1 with
    | true -> Both (perturb ~random_state t1, w1, t2, w2)
    | false -> Both (t1, w1, perturb ~random_state t2, w2))
  | Map (t, f) -> Map (perturb ~random_state t, f)
;;

let rec value : type a. a t -> a =
 fun t ->
  match t with
  | Const x -> x
  | Range_int (_, _, cur) -> cur
  | Both (t1, _, t2, _) -> value t1, value t2
  | Map (t, f) -> f (value t)
;;

let simulated_annealing perturbable ~score ~random_state ~temp_start ~n_steps =
  let cur_perturbable = ref perturbable in
  let cur_score = ref (score (value perturbable)) in
  let best_perturbable = ref perturbable in
  let best_score = ref (score (value perturbable)) in
  let n_promoted = ref 0 in
  let n_report_iters = ref 0 in
  let do_report () =
    print_s
      [%sexp
        "Report:"
        , { frac = (float !n_promoted /. float !n_report_iters : float)
          ; best_score = (!best_score : float)
          ; cur_score = (!cur_score : float)
          }];
    n_promoted := 0;
    n_report_iters := 0
  in
  for i = 0 to n_steps do
    let temp = temp_start *. (1. -. (float i /. float n_steps)) in
    let new_perturbable = perturb ~random_state !cur_perturbable in
    let new_score = score (value new_perturbable) in
    (* print_s [%sexp {cur_score = (!cur_score:float);new_score:float; temp:float}]; *)
    let difference = new_score -. !cur_score in
    if Float.( >= ) difference 0.
       || Float.( <= )
            (Random.State.float random_state 1.)
            (Float.exp (difference /. temp))
    then (
      cur_perturbable := new_perturbable;
      cur_score := new_score;
      incr n_promoted);
    if Float.( >= ) new_score !best_score
    then (
      best_perturbable := new_perturbable;
      best_score := new_score);
    incr n_report_iters;
    if i % 10000 = 0 then do_report ()
  done;
  !best_perturbable
;;
