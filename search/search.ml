open! Core

let main ~random_state =
  let gear_external_module = 2. in
  let gear_train_init =
    let open Perturbable.Let_syntax in
    let%map_open gear_a_contact_angle = random_angle
    and gear_b_c_module =
      let%map i = range ~lo:1700 ~hi:4000 in
      float i /. 1000.
    and gear_c_contact_angle = random_angle
    and gear_d_e_module =
      let%map i = range ~lo:1700 ~hi:4000 in
      float i /. 1000.
    and gear_e_contact_angle = random_angle
    and gear_m_contact_angle = random_angle
    and gear_w_contact_angle = random_angle
    and gear_x_y_module =
      let%map i = range ~lo:1700 ~hi:4000 in
      float i /. 1000.
    and gear_y_contact_angle = random_angle
    and gear_h_contact_angle = random_angle in
    let gear_s =
      Gear.create
        ~name:"s"
        ~n:92
        ~module_:gear_external_module
        ~parent:(Root Point.origin)
        ~external_:false
    in
    let gear_a =
      Gear.create
        ~name:"a"
        ~n:55
        ~module_:gear_external_module
        ~parent:(Contact { parent = gear_s; angle = gear_a_contact_angle })
        ~external_:true
    in
    let gear_b =
      Gear.create
        ~name:"b"
        ~n:11
        ~module_:gear_b_c_module
        ~parent:(Same_axle { parent = gear_a })
        ~external_:true
    in
    let gear_c =
      Gear.create
        ~name:"c"
        ~n:60
        ~module_:gear_b_c_module
        ~parent:(Contact { parent = gear_b; angle = gear_c_contact_angle })
        ~external_:true
    in
    let gear_d =
      Gear.create
        ~name:"d"
        ~n:15
        ~module_:gear_d_e_module
        ~parent:(Same_axle { parent = gear_c })
        ~external_:true
    in
    let gear_e =
      Gear.create
        ~name:"e"
        ~n:64
        ~module_:gear_d_e_module
        ~parent:(Contact { parent = gear_d; angle = gear_e_contact_angle })
        ~external_:true
    in
    let gear_f =
      Gear.create
        ~name:"f"
        ~n:16
        ~module_:gear_external_module
        ~parent:(Same_axle { parent = gear_e })
        ~external_:true
    in
    let gear_m =
      Gear.create
        ~name:"m"
        ~n:69
        ~module_:gear_external_module
        ~parent:(Contact { parent = gear_f; angle = gear_m_contact_angle })
        ~external_:false
    in
    let gear_w =
      Gear.create
        ~name:"w"
        ~n:55
        ~module_:gear_external_module
        ~parent:(Contact { parent = gear_f; angle = gear_w_contact_angle })
        ~external_:true
    in
    let gear_x =
      Gear.create
        ~name:"x"
        ~n:11
        ~module_:gear_x_y_module
        ~parent:(Same_axle { parent = gear_w })
        ~external_:true
    in
    let gear_y =
      Gear.create
        ~name:"y"
        ~n:54
        ~module_:gear_x_y_module
        ~parent:(Contact { parent = gear_x; angle = gear_y_contact_angle })
        ~external_:true
    in
    let gear_z =
      Gear.create
        ~name:"z"
        ~n:15
        ~module_:gear_external_module
        ~parent:(Same_axle { parent = gear_y })
        ~external_:true
    in
    let gear_h =
      Gear.create
        ~name:"h"
        ~n:46
        ~module_:gear_external_module
        ~parent:(Contact { parent = gear_z; angle = gear_h_contact_angle })
        ~external_:false
    in
    [ gear_s
    ; gear_a
    ; gear_b
    ; gear_c
    ; gear_d
    ; gear_e
    ; gear_f
    ; gear_m
    ; gear_w
    ; gear_x
    ; gear_y
    ; gear_z
    ; gear_h
    ]
  in
  let score_gear_train ~verbose gear_train =
    let gear_by_name =
      let gear_train_map =
        gear_train
        |> List.map ~f:(fun (gear : Gear.t) -> Gear.name gear, gear)
        |> String.Map.of_alist_exn
      in
      fun name -> Map.find_exn gear_train_map name
    in
    let centre_of_gravity gear_train =
      let cog_sum, cog_weight =
        List.fold
          gear_train
          ~init:(Point.origin, 0.)
          ~f:(fun (cog_sum, cog_weight) gear ->
            let r = Gear.pitch_radius gear in
            let gear_weight = Float.pi *. r *. r in
            ( Point.( + ) cog_sum (Point.( * ) (Gear.position gear) gear_weight)
            , cog_weight +. gear_weight ))
      in
      Point.( / ) cog_sum cog_weight
    in
    let cog_mm = Point.polar_radius (centre_of_gravity gear_train) in
    let total_invalid_axle_mm = ref 0. in
    let total_gear_sticking_out_mm = ref 0. in
    let total_hms_offset_mm = ref 0. in
    let total_axle_intersects_hms = ref 0. in
    let total_axle_intersects_other_axle = ref 0. in
    let total_e_axle_intersects_abcd = ref 0. in
    let score =
      List.fold gear_train ~init:0. ~f:(fun score gear ->
          let position = Gear.position gear in
          let invalid_axle_mm =
            Point.polar_radius
              { x = Float.max (Float.abs position.x -. 95.) 0.
              ; y = Float.max (Float.abs position.y -. 95.) 0.
              }
          in
          total_invalid_axle_mm := !total_invalid_axle_mm +. invalid_axle_mm;
          let gear_sticking_out_mm =
            Float.max (Point.polar_radius position +. Gear.pitch_radius gear -. 100.) 0.
          in
          total_gear_sticking_out_mm
            := !total_gear_sticking_out_mm +. gear_sticking_out_mm;
          let hms_offset_mm =
            match Gear.name gear with
            | "h" | "m" | "s" -> Point.polar_radius position
            | _ -> 0.
          in
          total_hms_offset_mm := !total_hms_offset_mm +. hms_offset_mm;
          let has_axle (gear : Gear.t) =
            Gear.external_ gear
            &&
            match Gear.parent gear with
            | Same_axle _ -> false
            | Root _ | Contact _ -> true
          in
          (* remaps 0--1 to 0|step--1 *)
          let magic_range ~step x =
            if Float.( <= ) x 0.
            then 0.
            else if Float.( >= ) x 1.
            then 1.
            else step +. (x *. (1. -. step))
          in
          let circle_circle_intersects ~o1 ~o2 ~r1 ~r2 =
            let a1 = Float.( < ) (Point.polar_radius (Point.( - ) o1 o2)) (r1 +. r2) in
            let a2 =
              1. -. (Point.polar_radius (Point.( - ) o1 o2) /. (r1 +. r2))
              |> magic_range ~step:0.4
            in
            assert (Bool.equal a1 (Float.( > ) a2 0.));
            a2
          in
          let circle_ring_intersects ~o1 ~o2 ~r1 ~r2i ~r2o =
            let a1 =
              let dist = Point.polar_radius (Point.( - ) o1 o2) in
              Float.( > ) dist (r2i -. r1) && Float.( < ) dist (r2o +. r1)
            in
            let a2 =
              let dist = Point.polar_radius (Point.( - ) o1 o2) in
              let square f = f *. f in
              -.(dist -. r1 -. r2o)
              *. (dist +. r1 -. r2i)
              /. square (((r2o -. r2i) /. 2.) +. r1)
              |> magic_range ~step:0.4
            in
            assert (Bool.equal a1 (Float.( > ) a2 0.));
            a2
          in
          let axle_intersects_hms =
            let test hms_gear =
              let hms_gear_radius = Gear.pitch_radius hms_gear in
              circle_ring_intersects
                ~o1:(Gear.position gear)
                ~r1:4.
                ~o2:(Gear.position hms_gear)
                ~r2i:(hms_gear_radius -. (1.2 *. gear_external_module))
                ~r2o:(hms_gear_radius +. (2.5 *. gear_external_module))
            in
            if has_axle gear
            then (
              let x =
                test (gear_by_name "h")
                +. test (gear_by_name "m")
                +. test (gear_by_name "s")
              in
              if verbose && Float.( > ) x 0.
              then print_s [%sexp (Gear.name gear : string), (x : float)];
              x)
            else 0.
          in
          total_axle_intersects_hms := !total_axle_intersects_hms +. axle_intersects_hms;
          let axle_intersects_other_axle =
            if has_axle gear
            then (
              let x =
                List.fold gear_train ~init:0. ~f:(fun accum other_gear ->
                    accum
                    +.
                    if (not (phys_equal gear other_gear)) && has_axle other_gear
                    then
                      circle_circle_intersects
                        ~o1:(Gear.position gear)
                        ~r1:4.
                        ~o2:(Gear.position other_gear)
                        ~r2:4.
                    else 0.)
              in
              if verbose && Float.( > ) x 0.
              then print_s [%sexp "intersect", (Gear.name gear : string), (x : float)];
              x)
            else 0.
          in
          total_axle_intersects_other_axle
            := !total_axle_intersects_other_axle +. axle_intersects_other_axle;
          let e_axle_intersects_abcd =
            let test other_gear =
              circle_circle_intersects
                ~o1:(Gear.position gear)
                ~r1:5.
                ~o2:(Gear.position other_gear)
                ~r2:(Gear.pitch_radius other_gear +. Gear.module_ other_gear)
            in
            if String.( = ) (Gear.name gear) "e"
            then
              test (gear_by_name "a")
              +. test (gear_by_name "c")
              +. test (gear_by_name "w")
              +. test (gear_by_name "y")
            else 0.
          in
          total_e_axle_intersects_abcd
            := !total_e_axle_intersects_abcd +. e_axle_intersects_abcd;
          let bonus =
            0.
            (* penalty: 100 per mm of axle outside [-100, 100] *)
            -. (100. *. invalid_axle_mm)
            (* penalty: 1 per mm of gear sticking outside [-105, 105] *)
            -. (8. *. gear_sticking_out_mm)
            (* penalty: 100 per mm away from origin for clock hand gears *)
            -. (500. *. hms_offset_mm)
            -. (50. *. axle_intersects_hms)
            -. (100. *. axle_intersects_other_axle)
            -. (400. *. e_axle_intersects_abcd)
          in
          (* print_s [%sexp { gear : Gear.Alternate_sexp.t ; position : Point.t; penalty : float }]; *)
          score +. bonus)
      (* penalty: 4 per mm away from CoG *)
      -. (2. *. cog_mm)
    in
    if verbose
    then
      print_s
        [%sexp
          { cog_mm : float
          ; total_invalid_axle_mm : float ref
          ; total_gear_sticking_out_mm : float ref
          ; total_hms_offset_mm : float ref
          ; total_axle_intersects_hms : float ref
          ; total_axle_intersects_other_axle : float ref
          ; total_e_axle_intersects_abcd : float ref
          }];
    score
  in
  let gear_train =
    Perturbable.simulated_annealing
      gear_train_init
      ~score:(score_gear_train ~verbose:false)
      ~random_state
      ~temp_start:100.
      ~n_steps:2_000_000
  in
  print_s
    [%sexp { gear_train = (Perturbable.value gear_train : Gear.Alternate_sexp.t list) }];
  print_s
    [%sexp
      { gear_train =
          (List.map (Perturbable.value gear_train) ~f:(fun gear ->
               gear, Gear.position gear) : (Gear.t * Point.t) list)
      ; score = (score_gear_train ~verbose:true (Perturbable.value gear_train) : float)
      }];
  ()
;;

let command =
  Command.basic
    ~summary:"_"
    (let open Command.Let_syntax in
    let%map_open seed = flag "-seed" (optional int) ~doc:"INT seed" in
    fun () ->
      let random_state =
        match seed with
        | None -> Random.State.make_self_init ()
        | Some seed -> Random.State.make [| seed |]
      in
      main ~random_state)
;;

let () = Command.run command
