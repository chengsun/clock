open! Core

type 'a t

val value : 'a t -> 'a

(** Maximises the score of the returned [Perturbable.t]. *)
val simulated_annealing
  :  'a t
  -> score:('a -> float)
  -> random_state:Base.Random.State.t
  -> temp_start:float
  -> n_steps:int
  -> 'a t

(** The subset of [Applicative.S] required for [let%map_open] to work. (Usually
    one would implement all of [Applicative.Basic], and derive this automatically
    using [Applicative.Make].) *)
module Let_syntax : sig
  module Let_syntax : sig
    type nonrec 'a t = 'a t

    val return : 'a -> 'a t
    val both : 'a t -> 'b t -> ('a * 'b) t
    val map : 'a t -> f:('a -> 'b) -> 'b t

    module Open_on_rhs : sig
      val return : 'a -> 'a t
      val range : lo:int -> hi:int -> int t
      val random_angle : float t
    end
  end
end
