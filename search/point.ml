open! Core

type t =
  { x : float
  ; y : float
  }
[@@deriving sexp_of]

let map t ~f = { x = f t.x; y = f t.y }
let map2 t1 t2 ~f = { x = f t1.x t2.x; y = f t1.y t2.y }
let ( + ) = map2 ~f:Float.( + )
let ( - ) = map2 ~f:Float.( - )
let ( * ) t scalar = map t ~f:(fun f -> f *. scalar)
let ( / ) t scalar = map t ~f:(fun f -> f /. scalar)
let of_angle angle = { x = Float.cos angle; y = Float.sin angle }
let of_polar ~angle ~radius = of_angle angle * radius
let origin = { x = 0.; y = 0. }
let polar_radius { x; y } = Float.hypot x y
