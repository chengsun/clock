#![no_main]
#![no_std]

use panic_halt as _;

use stm32f0xx_hal as hal;

use crate::hal::{
    gpio::*,
    pac::{interrupt, Interrupt, Peripherals, TIM2},
    prelude::*,
    time::Hertz,
    timers::*,
};

use cortex_m_rt::entry;

use core::cell::RefCell;
use cortex_m::{interrupt::Mutex, peripheral::Peripherals as c_m_Peripherals};

// A type definition for the GPIO pin to be used for our LED
type C6PIN = gpioc::PC6<Output<PushPull>>;
type C7PIN = gpioc::PC7<Output<PushPull>>;
type C8PIN = gpioc::PC8<Output<PushPull>>;
type C9PIN = gpioc::PC9<Output<PushPull>>;

// Make LED pin globally available
static GC6: Mutex<RefCell<Option<C6PIN>>> = Mutex::new(RefCell::new(None));
static GC7: Mutex<RefCell<Option<C7PIN>>> = Mutex::new(RefCell::new(None));
static GC8: Mutex<RefCell<Option<C8PIN>>> = Mutex::new(RefCell::new(None));
static GC9: Mutex<RefCell<Option<C9PIN>>> = Mutex::new(RefCell::new(None));

// Make timer interrupt registers globally available
static GINT: Mutex<RefCell<Option<Timer<TIM2>>>> = Mutex::new(RefCell::new(None));

// Define an interupt handler, i.e. function to call when interrupt occurs. Here if our external
// interrupt trips when the timer timed out
#[interrupt]
fn TIM2() {
    static mut C6: Option<C6PIN> = None;
    static mut C7: Option<C7PIN> = None;
    static mut C8: Option<C8PIN> = None;
    static mut C9: Option<C9PIN> = None;
    static mut INT: Option<Timer<TIM2>> = None;
    // (92/minute / 17 * 2048) / 8000Hz is 736/31875, which is approximately 1/43 - 1/6038
    // This approximation loses 1 step per 11 days.
    static mut N: u32 = 0;
    static mut N43: u32 = 0;
    static mut N6038: u32 = 0;
    static mut NEG_CARRY_6038: bool = false;

    let c6 = C6.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| GC6.borrow(cs).replace(None).unwrap())
    });

    let c7 = C7.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| GC7.borrow(cs).replace(None).unwrap())
    });

    let c8 = C8.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| GC8.borrow(cs).replace(None).unwrap())
    });

    let c9 = C9.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| GC9.borrow(cs).replace(None).unwrap())
    });

    let int = INT.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| GINT.borrow(cs).replace(None).unwrap())
    });

    *N43 += 1;
    *N6038 += 1;

    if *N6038 == 6038 {
        *N6038 = 0;
        *NEG_CARRY_6038 = true;
    }

    if *N43 == 43 {
        *N43 = 0;
        if *NEG_CARRY_6038 {
            *NEG_CARRY_6038 = false;
        } else {
            c6.set_low().ok();
            c7.set_low().ok();
            c8.set_low().ok();
            c9.set_low().ok();

            match *N {
                0 => {
                    c9.set_high().ok();
                    *N = 1;
                }
                1 => {
                    c8.set_high().ok();
                    *N = 2;
                }
                2 => {
                    c7.set_high().ok();
                    *N = 3;
                }
                3 => {
                    c6.set_high().ok();
                    *N = 0;
                }
                _ => {
                    *N = 0;
                }
            }
        }
    }

    int.wait().ok();
}

#[entry]
fn main() -> ! {
    let mut p = Peripherals::take().unwrap();
    let cp = c_m_Peripherals::take().unwrap();
    cortex_m::interrupt::free(move |cs| {
        let mut rcc = p.RCC.configure().sysclk(8.mhz()).freeze(&mut p.FLASH);

        let gpioc = p.GPIOC.split(&mut rcc);

        let c6 = gpioc.pc6.into_push_pull_output(cs);
        let c7 = gpioc.pc7.into_push_pull_output(cs);
        let c8 = gpioc.pc8.into_push_pull_output(cs);
        let c9 = gpioc.pc9.into_push_pull_output(cs);

        *GC6.borrow(cs).borrow_mut() = Some(c6);
        *GC7.borrow(cs).borrow_mut() = Some(c7);
        *GC8.borrow(cs).borrow_mut() = Some(c8);
        *GC9.borrow(cs).borrow_mut() = Some(c9);

        // Set up a timer
        let mut timer = Timer::tim2(p.TIM2, Hertz(8000), &mut rcc);

        // Generate an interrupt when the timer expires
        timer.listen(Event::TimeOut);

        // Move the timer into our global storage
        *GINT.borrow(cs).borrow_mut() = Some(timer);

        // Enable TIM2 IRQ, set prio 1 and clear any pending IRQs
        let mut nvic = cp.NVIC;
        unsafe {
            nvic.set_priority(Interrupt::TIM2, 1);
            cortex_m::peripheral::NVIC::unmask(Interrupt::TIM2);
        }
        cortex_m::peripheral::NVIC::unpend(Interrupt::TIM2);
    });

    loop {
        continue;
    }
}
